SET(SIMOUTGID_SRCS SimOutGiD.cc)

ADD_LIBRARY(simoutgid STATIC ${SIMOUTGID_SRCS})

ADD_DEPENDENCIES(simoutgid boost)

IF(TARGET cgal)
  ADD_DEPENDENCIES(simoutgid cgal)
ENDIF()

TARGET_LINK_LIBRARIES(simoutgid
  ${GIDPOST_LIBRARY}
  ${HDF5_LT_LIBRARY}
  ${HDF5_LIBRARY}
  ${ZLIB_LIBRARY}
  )
