<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/simulation" xmlns="http://www.cfs++.org/simulation"
  xmlns:sim="http://www.cfs++.org/simulation"
  elementFormDefault="qualified">


  <xsd:annotation>
    <xsd:documentation xml:lang="en"> Parameter file schema for the Coupled Field Data processing
      project CFS-Dat </xsd:documentation>
  </xsd:annotation>


  <!-- ******************************************************************* -->
  <!--   Includes from CFS -->
  <!-- ******************************************************************* -->
  <xsd:include schemaLocation="../CFS-Simulation/Schemas/CFS_Documentation.xsd"/>
  
  <!-- ******************************************************************* -->
  <!--   Includes from cfsdat -->
  <!-- ******************************************************************* -->  
  <xsd:include schemaLocation="Schemas/CFSDat_Differentiation.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_GaussDerivative.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_VolumeMultiplication.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_Aeroacoustic.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_BaseFilter.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_DatMeshInput.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_DatMeshOutput.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_TDeriv1.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_FIRFilter.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_RotSubstantialTimeDeriv.xsd"/> 
  <xsd:include schemaLocation="Schemas/CFSDat_Interpolation.xsd"/>  
  <xsd:include schemaLocation="Schemas/CFSDat_BinOpFilt.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_Tensor.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_PostLighthill.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_FftFilter.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_TemporalBlending.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_TMean.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_FilterList.xsd"/>
  <xsd:include schemaLocation="Schemas/CFSDat_SNGR.xsd"/>


  <xsd:group name="FilterGroup">
    <xsd:choice>
      <xsd:element name="meshInput" type="DT_DatMeshInput"/>
      <xsd:element name="meshOutput" type="DT_DatMeshOutput"/>
      <xsd:element name="timeDeriv1" type="DT_TDeriv1"/>
      <xsd:element name="fir" type="DT_FIRFilter"/>
      <xsd:element name="substantialDeriv" type="DT_RotSTDeriv"/>
      <xsd:element name="interpolation" type="DT_Interpolation"/>
      <xsd:element name="differentiation" type="DT_Differentiation"/>
      <xsd:element name="gaussDerivative" type="DT_GaussDerivative"/>
      <xsd:element name="volumeMultiplication" type="DT_VolumeMultiplication"/>
      <xsd:element name="aeroacoustic" type="DT_Aeroacoustic"/>
      <xsd:element name="postLighthill" type="DT_postLighthill"/>
      <xsd:element name="tensorFilter" type="DT_TensorFilter"/>
      <xsd:element name="binaryOperation" type="DT_BinOpFilt">
        <xsd:annotation>
          <xsd:documentation>
            This filter combines two given results according to the specified operation.
            opType may be plus,minus,mult,div
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="syntheticTurbulence" type="DT_SNGR"/>
      <xsd:element name="fft" type="DT_FFT"/>
      <xsd:element name="timeMean" type="DT_TMean">
        <xsd:annotation>
          <xsd:documentation>
            This filter computes the moving average of a time series with a defined span.
            With the span it is possible to control the filter property. This filter extracts
            the frequency content up to a maximum freuqency defined by the span.
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="temporalBlending" type="DT_TemporalBlend">
        <xsd:annotation>
          <xsd:documentation>
            This filter multiplies every timestep with a specified function f(t), therewith
            different blendings for e.g. acoustic source terms can be realized
          </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
    </xsd:choice>
  </xsd:group>



  <xsd:element name="cfsdat">
    <xsd:complexType>
      <xsd:sequence>
			  <!-- Tags for documenting simulations (required just for test suite)-->
        <xsd:element name="documentation" type="sim:DT_Documentation" minOccurs="0"/>
        <!-- File format list for io-specifications (required)-->
        <xsd:element name="pipeline" type="DT_FilterList" minOccurs="1" maxOccurs="1"/>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
</xsd:schema>
